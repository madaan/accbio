#!/usr/bin/env python

#load pickle-generated file of classifier and run

import csv
import cPickle
import numpy as np

file_classifier = open('nn_classifier_complete_k2000_weighted_distance.obj', 'rb')
nn_classifier = cPickle.load(file_classifier)

file_classifier.close()

questionsfile = open('testMeans-devices.csv', 'r')
questionsreader = csv.reader(questionsfile)

device_list_crude = open('../../data/devices').readlines()
device_list = []
for item in device_list_crude:
	device_list.append(int(item))

X_list = [] #2-D list for storing features
Y_list = [] #1-D lisit for storing target labels

for row in questionsreader:
		Y_list.append(int(row[3]))
		X_list.append(map(float, row[0:3]))#using X,Y,Z comp of Acceleration(Time not considered)

questionsfile.close()

X_array = np.array(X_list) #convert lists into numpy arrays

del X_list


for i in range(len(X_array)):
	predict_array = nn_classifier.predict_proba(X_array[i])
	index = device_list.index(Y_list[i]) #read index from the device_list matching Y_list
	print predict_array[0][index]
