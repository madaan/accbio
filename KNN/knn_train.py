#!/usr/bin/env python

#k-nearest neighbor classification

import csv
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import cPickle

#declarations
k=2000 #k in the k-nearest neighbors
train_set = '../../data/train.csv' #the training data file name 
classifier_dump_pickle = 'nn_classifier_complete_k2000_weighted_distance.obj' #the name of the object file that stores the classifier

trainfile = open(train_set, 'r')

datareader = csv.reader(trainfile)

X_list = [] #2-D list for storing features
Y_list = [] #1-D lisit for storing target labels 

#line_read =0

for row in datareader:
	if row[0].isdigit(): #ignore headers
		Y_list.append(int(row[4]))
		X_list.append(map(float, row[1:4]))#using X,Y,Z comp of Acceleration(Time not considered)
		#line_read += 1
		#print("Line reading:", line_read) 

trainfile.close()

Y_array = np.array(Y_list) #convert lists into numpy arrays
X_array = np.array(X_list) #convert lists into numpy arrays

del Y_list
del X_list

nn_classifier = KNeighborsClassifier(n_neighbors=k, weights='distance') #n_neighbors is the value of k
nn_classifier.fit(X_array, Y_array)

file_classifier = open(classifier_dump_pickle, 'w')
cPickle.dump(nn_classifier, file_classifier)
