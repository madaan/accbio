#!/usr/bin/python


#find the naive bayes estimates of test_avg_interval.csv using parameters trained in train_avg.csv.


import sys
import csv
import math
import numpy as np


def gaussian(x,mu,var):
        """ definition of the guassian function"""
        y = 1 / ( math.sqrt(2. * math.pi * var) )  * math.exp(-((x-mu+0.0)**2 / (2.*var)))
        return y


with open("../../data/questions.csv","rb") as csvQuestionsFile:
	csvQuestionReader = csv.reader(csvQuestionsFile,delimiter=',')

	with open("../../data/test_avg_interval.csv", 'rb') as csvTestfile:
        	csvTestReader = csv.reader(csvTestfile, delimiter=' ')

		print "creating the test dictionary"
		
		testDict = {}
		#creating testing data..
		testSeq = np.empty((15,3))
		ct = 0	
		for row in csvTestReader:
			if ct == 14 :	
				testSeq[ct] = [float(row[1]),float(row[2]),float(row[3])]
				testDict[int(row[0])] = testSeq
				ct = 0
			else:
				testSeq[ct] = [float(row[1]),float(row[2]),float(row[3])]
				ct = ct + 1

	with open("../../data/train_avg.csv",'rb') as csvTrainfile:
		csvTrainReader = csv.reader(csvTrainfile,delimiter=',')

		#creating training data
		
		print "creating the train dictionary"		

		trainDict = {}
		for row in csvTrainReader:
			trainDict[int(row[7])] = [float(row[0]),float(row[1]),float(row[2]),float(row[3]),float(row[4]),float(row[5]),int(row[6])]

	
	with open("submission_norm_interval.csv",'wb') as csvSubmissionfile:
		csvSubmissionWriter = csv.writer(csvSubmissionfile,delimiter=',')


		#write code from here....
		for row in csvQuestionReader:
			
			question_id = row[0]
			seq = int(row[1])
			seqProb = 0.0

			for devices in trainDict:
				device_id = int(devices)
				devData = trainDict[device_id]
				sequence = testDict[seq]

				prior_prob = 1.0 * (devData[6] / 29563983.)	

				seqProb = 0.0
				for seqData in sequence:
					prob = 1.
					prob = prob * gaussian(seqData[0],devData[0],devData[3]) 
					prob = prob * gaussian(seqData[1],devData[1],devData[4])
					prob = prob * gaussian(seqData[2],devData[2],devData[5])
					seqProb = seqProb + prob
				seqProb = (seqProb / 15) * prior_prob
				

			device_id = int(row[2])

			devData = trainDict[device_id]
			sequence = testDict[seq]
			
			prior_prob = 1.0 * (devData[6] / 29563983.)	
			
			test_seq_prob = 0.
			for seqData in sequence:
				prob = 1.
				prob = prob * gaussian(seqData[0],devData[0],devData[3]) 
				prob = prob * gaussian(seqData[1],devData[1],devData[4])
				prob = prob * gaussian(seqData[2],devData[2],devData[5])
				test_seq_prob = test_seq_prob + prob
			
			test_seq_prob = (test_seq_prob / 15) * prior_prob

			norm_prob = test_seq_prob / seqProb
			
			print "calculated question_id: ", question_id
            		csvSubmissionWriter.writerow([question_id,norm_prob])

