###DIR DESCRIPTION 

1. random : Purely random classification

2. nb : Naive bayes trained using sklearn on small train data and tested on means, giving AUC 78.66


3. kaggleForum : Solution based on a suggestion given on one of the forums.

4. distance based : Distance based approach for classification.

5. stochasticDescentLogLoss : Stochastic gradient descent based logistic regression.

6. logistic_regression : The sklearn's logistic regression

7. stochastic_gradient_descent : Stochastic gradient descent with hinge loss

8. DecisionTree : decision tree classification

9. AdaBoost : Boosting with DecisionTreeClassifier as base (Later will extend to naive bayes base and others)

###ADDING A NEW SOLUTION 


Please give an intuitive name to the folder and add a verbose description in the README. 

                  **PLEASE DO NOT REPEAT.**
